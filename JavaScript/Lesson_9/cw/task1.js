/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */


/*
Чтобы работало:

  1. Открыть в браузере ./index.html
  2. Смотреть в консоль

Если надо пересобрать: npm i; npm run cli
*/
import { layEggs } from './modules/layEggs'
import { fly } from './modules/fly'
import { swim } from './modules/swim'
import { eat } from './modules/eat'
import { hunt } from './modules/hunt'
import { sing } from './modules/sing'
import { deliverMail } from './modules/deliverMail'
import { run } from './modules/run'

class Bird {
  constructor(name)  {
    this.name = name
  }
  layEggs() { layEggs.call(this) }
  fly() { fly.call(this) }
  swim() { swim.call(this) }
  eat() { eat.call(this) }
  hunt() { hunt.call(this) }
  sing() { sing.call(this) }
  deliverMail() { deliverMail.call(this) }
  run() { run.call(this) }
}

const hawk = new Bird('Hawk')
hawk.layEggs()
hawk.fly()
hawk.swim()
hawk.eat()
hawk.hunt()
hawk.sing()
hawk.deliverMail()
hawk.run()

const penguin = new Bird('Penguin')
penguin.layEggs()
penguin.fly()
penguin.swim()
penguin.eat()
penguin.hunt()
penguin.sing()
penguin.deliverMail()
penguin.run()
