/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

// 1
function Comment (name, text, avatarUrl) {
  this.name = name
  this.text = text
  this.avatarUrl = avatarUrl
  this.likes = 0
}

var comment = new Comment('John', 'Hi', 'someUrl')

var defaultAvatarComment = {
  avatarUrl: 'https://asdasd.com/foo.jpg',
  addLike: function () {
    this.likes += 1
  }
}

Object.setPrototypeOf(defaultAvatarComment, comment);

// 2
var CommentsArray = [
  new Comment('John', 'Hi', 'someurl1'),
  new Comment('Jane', 'Hello', 'someurl2'),
  new Comment('Mark', 'Cya', 'someurl3'),
  new Comment('Maria', 'Welcome', 'someurl4')
]

// 3
function outputComments (comments) {
  var container = document.getElementById('CommentsFeed')
  for (var i = 0; i <  comments.length; i++) {
    var cmt = comments[i]
    var cmtNode = document.createElement('p')
    var cmtContent = document.createTextNode(cmt.name + ' ' + cmt.text)
    cmtNode.appendChild(cmtContent)
    container.appendChild(cmtNode)
  }
}

outputComments(CommentsArray)
