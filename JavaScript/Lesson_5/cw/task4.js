/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)
*/

var charsLow = 'abcdefghijklmnopqrstuvwxyz'
var charsUp = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

function encryptCesar (shift, word) {
  var letters = word.split('')
  var encrypted = []
  for (var i = 0; i < letters.length; i++) {
    var ltr = letters[i]
    var chars = charsLow.indexOf(ltr) > -1 ? charsLow : charsUp
    var newIdx = chars.indexOf(ltr) + shift
    if (newIdx >= chars.length) {
      newIdx -= chars.length
    }
    encrypted.push(chars[newIdx])
  }
  return encrypted.join('')
}

function decryptCesar (shift, word) {
  var letters = word.split('')
  var encrypted = []
  for (var i = 0; i < letters.length; i++) {
    var ltr = letters[i]
    var chars = charsLow.indexOf(ltr) > -1 ? charsLow : charsUp
    var newIdx = chars.indexOf(ltr) - shift
    if (newIdx < 0) {
      newIdx += chars.length
    }
    encrypted.push(chars[newIdx])
  }
  return encrypted.join('')
}

var encryptCesar1 = encryptCesar.bind(null, 1)
var encryptCesar2 = encryptCesar.bind(null, 2)
var encryptCesar3 = encryptCesar.bind(null, 3)
var encryptCesar4 = encryptCesar.bind(null, 4)
var encryptCesar5 = encryptCesar.bind(null, 5)

var decryptCesar1 = decryptCesar.bind(null, 1)
var decryptCesar2 = decryptCesar.bind(null, 2)
var decryptCesar3 = decryptCesar.bind(null, 3)
var decryptCesar4 = decryptCesar.bind(null, 4)
var decryptCesar5 = decryptCesar.bind(null, 5)

var wrd = 'AbcHelloWorldZoomXyz'
console.log(decryptCesar(10, encryptCesar(10, wrd)))
console.log(decryptCesar1(encryptCesar1(wrd)))
console.log(decryptCesar2(encryptCesar2(wrd)))
console.log(decryptCesar3(encryptCesar3(wrd)))
console.log(decryptCesar4(encryptCesar4(wrd)))
console.log(decryptCesar5(encryptCesar5(wrd)))
