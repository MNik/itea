/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

	var train = {};
    train.name = 'Intercity';
    train.speed = 200;
    train.passengers = 100;
    train.drive = function(){
      console.log('Поезд ' + this.name + ' везет ' + this.passengers + ' пасажиров со скоростью ' + this.speed + ' км/ч')
    }
    train.stop = function(){
      this.speed = 0;
      console.log('Поезд ' + this.name + ' остановился ' + this.speed + ' км/ч')
    }
    train.add = function(addNum){
      this.passengers += addNum;
      console.log('Кол-во пассажиров ' + this.passengers)
    }
    train.drive()
    train.stop()
    train.add(15)
