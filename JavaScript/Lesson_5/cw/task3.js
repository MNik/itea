/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function Dog (name, breed) {
  this.name = name
  this.breed = breed
  this.status = 'sleeping'
  this.run = function() {
    this.status = 'running'
    console.log('Dog named', this.name, 'is', this.status)
  }
  this.eat = function() {
    this.status = 'eating'
    console.log('Dog named', this.name, 'is', this.status)
  }
  this.describe = function() {
    console.log('Dog description:')
    for (key in this) {
      var value = this[key]
      if (typeof value === 'function') {
        console.log('It can', key)
      } else {
        console.log('Its', key, 'is', value)
      }
    }
  }
}

var muhtar = new Dog('Muhtar', 'Shepard')
muhtar.run()
muhtar.eat()
muhtar.describe()
