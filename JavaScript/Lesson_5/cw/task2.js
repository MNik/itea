/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/


document.addEventListener('DOMContentLoaded', function(){
  // 1
  function editPage1 (color) {
    document.body.style.color = color
    document.body.style.background = this.background
    console.log(
      'New color', document.body.style.color,
      'New bg', document.body.style.background)

    var title = document.createElement('h1')
    title.innerText = "I know how binding works in JS"
    document.body.appendChild(title)
  }
  editPage1.call({background: 'white'}, 'purple')

  // 2
  function editPage2 () {
    document.body.style.color = this.color
    document.body.style.background = this.background
    console.log(
      'New color', document.body.style.color,
      'New bg', document.body.style.background)

    var title = document.createElement('h1')
    title.innerText = "I know how binding works in JS"
    document.body.appendChild(title)
  }
  editPage2 = editPage2.bind({background: 'black', color: 'red'})
  editPage2()

  // 3
  function editPage3 (headerText) {
    document.body.style.color = this.color
    document.body.style.background = this.background
    console.log(
      'New color', document.body.style.color,
      'New bg', document.body.style.background)

    var title = document.createElement('h1')
    title.innerText = headerText
    document.body.appendChild(title)
  }
  editPage3.apply(
    {background: 'green', color: 'orange'},
    ["I know how binding works in JS"])
})
