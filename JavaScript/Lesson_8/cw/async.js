/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
async function showCompanies() {
  const companiesResp = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2')
  const companies = await companiesResp.json()
  const table = document.getElementById('companies-table')
  companies.forEach((comp) => {
    let tr = document.createElement('tr')

    let td1 = document.createElement('td')
    td1.innerText = comp.company
    let td2 = document.createElement('td')
    td2.innerText = comp.balance

    let td3 = document.createElement('td')
    let btn3 = document.createElement('button')
    btn3.dataset.secret = comp.registered
    btn3.innerText = 'Показать'
    btn3.onclick = showSecretContent
    td3.appendChild(btn3)

    let td4 = document.createElement('td')
    let btn4 = document.createElement('button')
    let addr = [
      comp.address.country,
      comp.address.city,
      comp.address.state,
      comp.address.street,
      comp.address.house
    ]
    btn4.dataset.secret = addr.join(', ')
    btn4.innerText = 'Показать'
    btn4.onclick = showSecretContent
    td4.appendChild(btn4)

    tr.appendChild(td1)
    tr.appendChild(td2)
    tr.appendChild(td3)
    tr.appendChild(td4)

    table.appendChild(tr)
  })
}

function showSecretContent () {
  this.parentElement.innerText = this.dataset.secret
}

document.addEventListener('DOMContentLoaded', function(){
  document.getElementById('companies-table').hidden = false
  showCompanies()
})
