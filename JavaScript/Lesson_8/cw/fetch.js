/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

function getResponseJson (response) {
  return response.json()
}

function displayUserData (userData) {
  const uname = document.getElementById('fetch-username')
  uname.innerText = userData.name + ' friends:'
  const ul = document.getElementById('fetch-user-friends')
  userData.friends.forEach((friend) => {
    let li = document.createElement('li')
    li.innerText = friend.name
    ul.appendChild(li)
  })
}

document.addEventListener('DOMContentLoaded', function(){
  document.getElementById('fetch-container').hidden = false
  fetch('http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2')
    .then(getResponseJson)
    .then((peopleJson) => {
      return peopleJson[new Date().getSeconds() % peopleJson.length]
    })
    .then((personJson) => {
      return fetch('http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2')
        .then(getResponseJson)
        .then((friendsJson) => {
          return {
            name: personJson.name,
            friends: friendsJson[0].friends
          }
        })
    })
    .then(displayUserData)
})


