
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
    2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели
       и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Array

*/

function stringifyJson(form) {
  let obj = {}
  const inputs = Array.from(form.getElementsByTagName('input'))
  inputs.forEach((inp) => {
    obj[inp.name] = inp.value
  })
  console.log(JSON.stringify(obj))
  return false
}

function parseJson(form) {
  const inp = form.querySelector('[name="jsonString"]')
  try {
    console.log(JSON.parse(inp.value))
  } catch (e) {
    console.log('Не удалось спарсить JSON:', inp.value)
  }
  return false
}

document.addEventListener('DOMContentLoaded', function(){
  document.getElementById('json-container').hidden = false
})
