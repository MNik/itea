var butt = document.getElementsByClassName('showButton'),
    win = document.getElementsByClassName('tab');
hideAllTabs = function(event) {
  let win = document.getElementsByClassName('tab');
  win[0].classList.remove('active');
  win[1].classList.remove('active');
  win[2].classList.remove('active');
}

butt[0].onclick = function(){
  hideAllTabs();
  win[0].classList.add('active');
}
butt[1].onclick = function(){
  hideAllTabs();
  win[1].classList.add('active');
}
butt[2].onclick = function(){
  hideAllTabs();
  win[2].classList.add('active');
}


  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
